import {of, Observable} from 'rxjs';

export const testObservable: Observable<string> = of('hello from index.ts');
